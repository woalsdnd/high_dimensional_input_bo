function acq = SEard_acq(model, x)
%% return acquisition function (GP-UCB) for k^i
%
[mu, var] = SEard_mean_var(model, x);

sigma = sqrt(var);

% GP-UCB
% coeff is following the continuous case in the original GP-UCB paper
delta = 0.1;t=model.n;d=model.projection_dim;r=2;a=1;b=1;
coeff = 2*log(t^2*2*pi^2/(3*delta))+2*d*log(t^2*d*b*r*sqrt(log(4*d*a/delta)));
acq = mu+sqrt(coeff)*sigma;
end
