function result = interaction_term_dcop(model,k_xx_i,k_xx_j,k_Nx_i,k_Nx_j,VARreduction_i,VARreduction_j)
% return interaction terms between ith variable and jth variable
% in final approximate anova-UCB 
% 
% INPUT : k^(i)(x^(i),x^(i)), k^(i)(x^(i),X^(i)),
%         k^(j)(x^(j),x^(j)), k^(j)(x^(j),X^(j))
%         VARreduction_i=k^(i)(x^(i),X^(i))*K^{-1}*k^(i)(x^(i),X^(i))'+2*k^(i)(x^(i),X^(i))*K^{-1}*1
%         VARreduction_j=k^(j)(x^(j),X^(j))*K^{-1}*k^(j)(x^(j),X^(j))'+2*k^(j)(x^(j),X^(j))*K^{-1}*1
%
% 
% OUTPUT = a joint function 
%

if ~strcmp(model.kernel_type,'anova'),error('the model is not set to anova'); end

% sqaure of signal, D
sf2=exp(2*model.hyp(end));
D=model.high_dim;

% calculate k^(ij)(x^(ij),x^(ij)), k^(ij)(x^(ij),X^(ij))
% and compensate for the variance
k_xx_ij=k_xx_i.*k_xx_j/sf2;                           
k_Nx_ij=k_Nx_i.*k_Nx_j/sf2;

% calculate posterior mean k^(ij)(x^(ij),X^(ij))*K^{-1}*(y-mu)
post_mean=model.prior_mean+ k_Nx_ij'*model.K_inv_f;

% calculate vairance terms
% term1'=1+k^(i)(x^(i),X^(i))+k^(j)(x^(j),X^(j))+k^(ij)(x^(ij),X^(ij))
% term2=term1'*K^{-1}*term1
% VARreduction_i=k^(i)(x^(i),X^(i))*K^{-1}*k^(i)(x^(i),X^(i))'+2*k^(i)(x^(i),X^(i))*K^{-1}*1
% VARreduction_j=k^(j)(x^(j),X^(j))*K^{-1}*k^(j)(x^(j),X^(j))'+2*k^(j)(x^(j),X^(j))*K^{-1}*1
term1=1+k_Nx_i+k_Nx_j+k_Nx_ij;
intermediate2=model.L'\(model.L\term1);
term2=term1'*intermediate2;

% collect variance
var=k_xx_ij-term2+VARreduction_i+VARreduction_j;

% multiply beta and compensate for signal
coeff=anova_coeff(model);
result=post_mean+sqrt(coeff)*var/sqrt(sf2*D*(D+1)/2);

end

