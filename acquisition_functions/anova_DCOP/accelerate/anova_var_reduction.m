function anova_var_reduction = anova_var_reduction(model,k_iD)
% return variance reduction term for one variable in anova-dcop
% 
%    INPUT : model,
%            k_iD  -> NX1 matrix of k^(i)(X^(i),x^(i))
%
%    OUTPUT : k^(i)(x^(i),X^(i))*K^{-1}*k^(i)(X^(i),x^(i))
%               +2*k^(i)(x^(i),X^(i))*K^{-1}*one_vector
%       
%             (one_vector=sf2*1)
%

% one_vector : sf2*1
sf2=exp(2*model.hyp(end));
one_vector=sf2*ones(size(model.X,1),1);

% intermediate : K^{-1}*k^(i)(X^(i),x^(i))
% term1=k^(i)(x^(i),X^(i))*K^{-1}*k^(i)(X^(i),x^(i))
% term2=2*one_vector'*K^{-1}*k^(i)(X^(i),x^(i))
intermediate=model.L'\(model.L\k_iD);
term1=k_iD'*intermediate;
term2=2*one_vector'*intermediate;

anova_var_reduction=term1+term2;

end

