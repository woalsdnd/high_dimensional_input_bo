function coeff = anova_coeff(model)
% return coefficient of UCB for anova kernel function
%

delta=0.1;
D=model.high_dim;   
t=model.n; 
coeff = 2*D*log(model.discretization*t/delta);

end

