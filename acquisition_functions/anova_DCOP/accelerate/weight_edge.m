function weight = weight_edge(model,i,j)
% return the weight of an edge connecting variable i and j 
% in the factor graph 
% (factor graph : a complete graph whose nodes are variables)
% formula: w_{ij}=max_{x_i,x_j}f_{ij}-min_{x_i,x_j}f_{ij}
%

maxproblem.f = @(x)-interaction_term(model,x(1),x(2),i,j);
[maxp_fmin,xmin,history]= direct(maxproblem, model.bounds([i j],:));

minproblem.f = @(x)interaction_term(model,x(1),x(2),i,j);
[minp_fmin,xmin,history]= direct(minproblem, model.bounds([i j],:));

weight=-maxp_fmin-minp_fmin;

end

