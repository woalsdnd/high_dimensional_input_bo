function y=mccorm(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-1.5,4]X[-3,4]
bounds = [-1.5,4; -3,4];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';

x1 = x(1);
x2 = x(2);

term1 = sin(x1 + x2);
term2 = (x1 - x2)^2;
term3 = -1.5*x1;
term4 = 2.5*x2;

y = term1 + term2 + term3 + term4 + 1;

end



