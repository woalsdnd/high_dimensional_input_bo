function y=rosen(x)

d = length(x);

%rescale the domain to [-5,10]^*
bounds = [-5,10];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

sum = 0;
for ii = 1:(d-1)
	xi = x(ii);
	xnext = x(ii+1);
	new = 100*(xnext-xi^2)^2 + (xi-1)^2;
	sum = sum + new;
end

y = sum;

end



