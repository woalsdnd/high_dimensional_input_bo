function y=dixonpr(x)

d = length(x);

%rescale the domain to [-10,10]^*
bounds = [-10,10];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));


x1 = x(1);
term1 = (x1-1)^2;

sum = 0;
for ii = 2:d
	xi = x(ii);
	xold = x(ii-1);
	new = ii * (2*xi^2 - xold)^2;
	sum = sum + new;
end

y = term1 + sum;

end



