function y=eggpack(x)

d = length(x);

%put values inside the range
for i=1:d
    if x(i)>1,x(i)=1;end
    if x(i)<-1,x(i)=-1;end
end

mult = 1;
cos_ = 0;
for ii = 1:d
	xi = x(ii);
    mult=mult*abs(xi);
    cos_=cos_+cos(5*pi/2*xi);
end

t=power(mult,1/d);

y = t*cos_/d;

end



