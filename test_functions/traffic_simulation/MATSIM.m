function y=MATSIM(x)
    variable_file='tolls';
    file=fopen(variable_file,'w');
    fprintf(file,'%1.1f\n',3*(x+ones(1,length(x))));%ERP prices vary between $0 and $6
    tic;
    [status, result] = system(['python simulateToll.py ' , variable_file],'-echo');
    fprintf('Time elapsed : %d sec\n',toc);
    y=str2num(result);
end
