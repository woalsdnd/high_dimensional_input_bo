import subprocess
import time
import os
import sys
from IPython.utils.io import stdout, stderr
import xml.etree.cElementTree as ET

def simulateToll(var_file):
    
    project_folder = "/home/jaemin/workspace_java/MATSim_RoadPricing"
    
    # load toll links
    lines = [line.rstrip('\n') for line in open(project_folder + "/config/toll_links")]
    
    # load variables
    x = [line.rstrip('\n') for line in open(var_file)]
    
    # create xml structure
    root = ET.Element("roadpricing", type="link", name="singapore erp")
    links = ET.SubElement(root, "links")
    
    # specify the price according to var_file
    i = 0
    for link in lines:
        link_tuple = ET.SubElement(links, "link", id=str(link))
        ET.SubElement(link_tuple, "cost", start_time="07:30", end_time="09:00", amount=str(x[i]))  # only care about morning rush hours 
        i = i + 1
    
    # output into xml    
    xml = ET.ElementTree(root)
    with open(project_folder + '/config/toll.xml', 'w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8" ?><!DOCTYPE roadpricing SYSTEM "http://www.matsim.org/files/dtd/roadpricing_v1.dtd">')
        xml.write(f, 'utf-8')
    
    # run MATSIM
    cp = "%s:%s/libs/*" % (project_folder, project_folder)
    config = "%s/config/config.xml" % (project_folder)
    subprocess.call("java -Xmx2g -cp %s org.matsim.run.Controler %s" % (cp, config),
    shell=True,stdout=open("MATSIM_log","w"),stderr=open("MATSIM_err_log","w"))
    
    # read scores from the file (max of last two scores to cover the case of decrement)
    lines = [line.rstrip('\n') for line in open(project_folder + "/MATSIM/matsim_output/scorestats.txt")]
    score = float(lines[len(lines) - 1].split("\t")[1])
    lines = [line.rstrip('\n') for line in open(project_folder + "/MATSIM/matsim_output/traveldistancestats.txt")]
    distance =  float(lines[len(lines) - 1].split("\t")[1])

    hourly_penalty=79.02;
    dist_penalty=0.000294;
    result=(-score-distance*dist_penalty)/(2*hourly_penalty);
    print result
    
    # move the result folder    
    os.rename(project_folder + "/MATSIM/matsim_output", project_folder + "/MATSIM/matsim_output_%f" % (time.clock()))
        
    
if __name__ == '__main__':
    simulateToll(sys.argv[1])
