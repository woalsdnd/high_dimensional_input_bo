function y=mixtureGaussian(x,Q)
        %rotate the basis
        x=x*Q;

        %set value
        mu1=-0.5; var1=0.3;
        mu2=0.5; var2=0.7;
        value = (1/sqrt(2*pi*var1)) * exp(-norm(x-mu1)^2/var1) +(1/sqrt(2*pi*var2)) * exp(-norm(x-mu2)^2/var2);

        %flip the sign to turn into a minimization problem
        y=-value;
end

