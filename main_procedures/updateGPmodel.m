function updated_model=updateGPmodel(model)
% update Maximum Spanning Tree, lower cholesky matrix, maximum value, maximum point, history
%        (also anova kernel functions)   
%

% update max value and maximizer
[model.max_val, argmax]= max(model.f);
model.max_x=model.X(argmax,:);
model.history_opt=[model.history_opt;[model.max_x model.max_val]];

% update hypers periodically
if mod(model.n,model.hyp_update_cycle)==0
    % learn hyper parameters 
    tic;
    model.hyp=learn_hyperparameters(model);
    fprintf('Learning Hypers : %d sec\n',toc);
end

% update maximum spanning tree periodically for anova-dcop
if mod(model.n,model.mst_update_cycle)==0 && strcmp(model.kernel_type,'anova')
    % build a maximum spanning tree (sparse matrix)
    tic;
    if model.high_dim~=2
        [model.edges,model.mst]=buildMaximumSpanningTree(model);  
    end
    fprintf('Building spanning tree : %d sec\n',toc);
end

% update lower cholesky and kernel matrices if necessary
if mod(model.n,model.hyp_update_cycle)==0 || (mod(model.n,model.mst_update_cycle)==0 && strcmp(model.kernel_type,'anova')) || size(model.L,1)==0
    % update lower cholesky
    if strcmp(model.kernel_type,'anova')
        cov = model.cov_model(model.hyp,model.X,model.X, 0,0,model.edges) + exp(2*model.noise)*eye(model.n);
    elseif strcmp(model.kernel_type,'additive')
        cov = model.cov_model(model.hyp,model.X,model.X, 0) + exp(2*model.noise)*eye(model.n);
    elseif strcmp(model.kernel_type,'ard')
        cov = model.cov_model(model.hyp,model.X,model.X) + exp(2*model.noise)*eye(model.n);
    end
    model.L = chol(cov, 'lower');
    
    % calculate kernel functions and K^{-1}(f-mu) in anova-dcop
    if strcmp(model.kernel_type,'anova')
        tic;
        for d=1:model.high_dim
            model.k_i{d}=arrayfun(@(xd) anova_cov_matrix(model,xd,0,d,0,model.edges,'auto'), model.X_discrete);   
            model.k_iD{d}=arrayfun(@(xd) anova_cov_matrix(model,xd,0,d,0,model.edges,'cross'), model.X_discrete,'UniformOutput',false);   
            model.k_iVARreduction{d}=arrayfun(@(i) anova_var_reduction(model,model.k_iD{d}{i}), 1:length(model.X_discrete));
        end
        model.K_inv_f=model.L'\(model.L\(model.f-model.prior_mean));
        fprintf('Calculating Kernel matrices : %d sec\n',toc);
    end
else
    % append new row on the previous cholesky matrix
    prev_X = model.X(1:model.n-1,:);
    new_x=model.X(model.n,:);
    if strcmp(model.kernel_type,'anova')
        cross_cov = model.cov_model(model.hyp,prev_X,new_x, 0,0,model.edges);
        auto_cov=model.cov_model(model.hyp,new_x,new_x, 0,0,model.edges)+exp(2*model.noise);
    elseif strcmp(model.kernel_type,'additive')
        cross_cov = model.cov_model(model.hyp,prev_X,new_x, 0);
        auto_cov = model.cov_model(model.hyp,new_x,new_x, 0)+exp(2*model.noise);
    elseif strcmp(model.kernel_type,'ard')
        cross_cov = model.cov_model(model.hyp,prev_X,new_x);
        auto_cov = model.cov_model(model.hyp,new_x,new_x)+exp(2*model.noise);
    end
    new_column = model.L\cross_cov;
    new_corner = sqrt(auto_cov - new_column'*new_column);
    model.L = [[model.L, zeros(model.n-1, 1)];[new_column', new_corner]];

    % append k_iD and update k_iVARreduction, K^{-1}(f-mu) in anova-dcop
    if strcmp(model.kernel_type,'anova')
        for d=1:model.high_dim
            for m=1:length(model.X_discrete)
                x=zeros(1,model.high_dim);    x(1,d)=model.X_discrete(m);
                model.k_iD{d}{m}=[model.k_iD{d}{m};model.cov_model(model.hyp,model.X(end,:),x, d,0,model.edges)];   
            end
            model.k_iVARreduction{d}=arrayfun(@(i) anova_var_reduction(model,model.k_iD{d}{i}), 1:length(model.X_discrete));
        end
        model.K_inv_f=model.L'\(model.L\(model.f-model.prior_mean));
    end
    
end

updated_model=model;

end