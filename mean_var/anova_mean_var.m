function [mean, var] = anova_mean_var(model, x,i,j,edges)

% INPUT
% model : current gp model
% x : evaluation point (each point is 1XD row vector)
% i,j : index of anova kernel (when i==0 && j==0, full covariance)
%
% OUTPUT 
% mean and variance of f^(ij)(x) given data (except noise)
%
% VAR
% k_xx : kernel matrix of the point
% k_Nx : kernel matrix between data and the point 
%
if ~strcmp(model.kernel_type,'anova'),error('the model is not set to anova'); end

k_xx = model.cov_model(model.hyp, x, x,i,j,edges);
k_Nx = model.cov_model(model.hyp, model.X, x,i,j,edges);

intermediate=model.L'\(model.L\k_Nx);

mean = model.prior_mean+intermediate'*(model.f-model.prior_mean);
var = k_xx - k_Nx'*intermediate;
end