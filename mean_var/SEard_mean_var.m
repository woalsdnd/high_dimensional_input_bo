function [mean, var] = SEard_mean_var(model, x)

% INPUT
% model : current gp model
% x : evaluation point (each point is 1XD row vector)
%
% OUTPUT 
% mean and variance of f^(i)(x) given data (except noise)
%
% VAR
% k_xx : kernel matrix of the point
% k_Nx : kernel matrix between data and the point 
%
if ~strcmp(model.kernel_type,'ard'),error('the model is not set to ard'); end

k_xx = model.cov_model(model.hyp, x, x);
k_Nx = model.cov_model(model.hyp, model.X, x);

intermediate=model.L'\(model.L\k_Nx);

mean = model.prior_mean+intermediate'*(model.f-model.prior_mean);
var = k_xx - k_Nx'*intermediate;
end
