function optimum=get_optimum(fun_name)
% return the optimum value of the MAXIMIZATION problem
%
%   INPUT : function name
%
%   OUTPUT : optimal value
%

if strcmp(fun_name,'branin')
    optimum=-0.397887;
elseif strcmp(fun_name,'schwef')
    optimum=0;
elseif strcmp(fun_name,'eggpack')
    optimum=0.41869;
elseif strcmp(fun_name,'logsumSE')
    optimum=99*log(9);
end

end
