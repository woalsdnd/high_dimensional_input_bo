% result_folder=[getenv('HOME_DIR') 'results/branin/'];
% result_folder=[getenv('HOME_DIR') ];
% file1=[result_folder 'result.mat'];
% file2=[result_folder 'result2.mat'];
% load(file1);
% load(file2);
% T=(0:299)';    T(1)=1;
% 
% regret=avr_cumulative_regret{1,5}.*T;
% avr_cumulative_regret=zeros(300,1);
% for i=1:300
%     avr_cumulative_regret(i)=sum(regret(1:i))/T(i);
% end



% t=a.simple_regret(:,2);
% simple_regret(:,2)=a.simple_regret(:,2);
% avr_cumulative_regret(:,2)=a.avr_cumulative_regret(:,2);
% 
% 
% save([result_folder 'result1.mat'],'simple_regret','avr_cumulative_regret');

% set input, output path 
fun_name='branin';
result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
% file_othermethods=[result_folder 'result.mat'];

file_othermethods=[getenv('HOME_DIR') 'results/server/branin/result21.mat'];

% read data of method 1-4 from a file
load(file_othermethods);

% remove the last element (legacy of old version)
% for i=1:20
%     for j=1:4
%         simple_regret{i,j}=simple_regret{i,j}(1:end-1);
%         avr_cumulative_regret{i,j}=avr_cumulative_regret{i,j}(1:end-1);
%     end   
% end

% load anova-dcop
anova_results=[];
for i=1:20
    anova_dcop_file=[getenv('HOME_DIR') 'results/server/branin/result' num2str(i) '.mat'];
    anova_result=load(anova_dcop_file);
    simple_regret{i,5}=anova_result.simple_regret{5};
    avr_cumulative_regret{i,5}=anova_result.avr_cumulative_regret{5};
end

save([result_folder 'result_merged.mat'],'simple_regret','avr_cumulative_regret');
