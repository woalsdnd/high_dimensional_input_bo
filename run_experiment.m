function run_experiment(fun_name,n_runs,n_iterations,n_init_pts,high_dim,projection_dim,hyp_update_cycle,mst_update_cycle,exp_num,plotIteration)
%% set output directory ,experiment setting function handler, objective function
fh=str2func(['exp_set_' fun_name]);
if strcmp(fun_name,'matsim')
    objective_function=MATSIM;
    high_dim=77;
else
    objective_function=analytic_fun(fun_name,high_dim);
end        
%% iterate methods
% 0:original BO
% 1:random basis 
% 2:random projection
% 3:subspace_learning
% 4:additive model
% 5:anova dcop
for run=1:n_runs
    for method=5:5
        % initialize a model with the experiment settings 
        model=fh(method,objective_function,n_init_pts,n_iterations,high_dim,projection_dim,hyp_update_cycle,mst_update_cycle);
        
        % run Bayesian Optimization
        [X,Y,opt_pts,opt_values]=BayesianOptimization(model,plotIteration);

        % save the results
        save_results(fun_name,Y,opt_values,run,method,exp_num);
    end
end

end
